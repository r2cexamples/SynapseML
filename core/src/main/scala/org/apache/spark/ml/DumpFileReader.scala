package org.apache.spark.ml

import java.io.{File, FileInputStream, InputStream}
import org.apache.commons.compress.archivers.dump.DumpArchiveEntry
import org.apache.commons.compress.archivers.dump.DumpArchiveInputStream

object DumpFileReader {
  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      println("Usage: DumpFileReader <path_to_dump_file>")
      sys.exit(1)
    }

    val dumpFilePath = args(0)

    try {
      val dumpFile = new File(dumpFilePath)
      if (!dumpFile.exists()) {
        println(s"File not found: $dumpFilePath")
        sys.exit(1)
      }

      val inputStream: InputStream = new FileInputStream(dumpFile)
      val dumpInputStream = new DumpArchiveInputStream(inputStream)

      var entry: DumpArchiveEntry = dumpInputStream.getNextDumpEntry
      while (entry != null) {
        if (entry.isDirectory) {
          println(s"Directory: ${entry.getName}")
        } else {
          println(s"File: ${entry.getName}, Size: ${entry.getSize} bytes")

          // Reading file content (if needed)
          val buffer = new Array[Byte](1024)
          var bytesRead = 0
          val content = new StringBuilder
          while ( { bytesRead = dumpInputStream.read(buffer); bytesRead } != -1) {
            content.append(new String(buffer, 0, bytesRead))
          }
          println(s"Content (truncated): ${content.toString.take(100)}...")
        }
        entry = dumpInputStream.getNextDumpEntry
      }

      dumpInputStream.close()
    } catch {
      case e: Exception =>
        e.printStackTrace()
    }
  }
}
